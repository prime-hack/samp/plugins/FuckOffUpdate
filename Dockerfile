FROM srteam/mxeposixuc:upd

RUN apt install curl zip ccache gnupg xz-utils -y && apt clean

RUN curl -fsS https://dlang.org/install.sh | bash -s dmd
