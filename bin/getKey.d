#!rdmd

import std.stdio;
import std.json;
import std.file;

void main(string[] args){
    if (args.length < 2) {
        writeln( "0" );
        return;
    }
    try{
        auto j = args[1].readText.parseJSON;
        writeln( j["key"].str );
        return;
    } catch ( Exception e ){}
    writeln( "0" );
}
