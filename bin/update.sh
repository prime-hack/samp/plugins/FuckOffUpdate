#!/bin/bash

# upload.sh api-key post-id filename filepath

source ~/dlang/dmd-2.097.2/activate

curl -X GET "https://blast.hk/api/posts/${2}/" -H "XF-Api-Key: ${1}" -o $CI_PROJECT_DIR/bin/post.json &> /dev/null

ID=`rdmd $CI_PROJECT_DIR/bin/getId.d $CI_PROJECT_DIR/bin/post.json "${3}"`

if [ "$ID" -eq "0" ]; then
	echo "post without attachment";
else
	echo "delete attachment";
	curl -X DELETE "https://blast.hk/api/attachments/${ID}/" -H "XF-Api-Key: ${1}" &> $CI_PROJECT_DIR/bin/delete.log
fi

curl -X POST "https://blast.hk/api/attachments/new-key" -H "XF-Api-Key: ${1}" -F "type=post" -F "context[post_id]=${2}" -F "attachment=@${4}" -o $CI_PROJECT_DIR/bin/key.json &> $CI_PROJECT_DIR/bin/key.log

KEY=`rdmd $CI_PROJECT_DIR/bin/getKey.d $CI_PROJECT_DIR/bin/key.json`

if [[ "$KEY" == "0" ]]; then
	echo "Invalid key ID";
else
	echo "Assign attach to post";
	curl -X POST "https://blast.hk/api/posts/${2}/" -H "XF-Api-Key: ${1}" -F "attachment_key=${KEY}" &> $CI_PROJECT_DIR/bin/assign.log
fi

deactivate

#echo "|"
#echo "DELETE LOG:"
#cat $CI_PROJECT_DIR/bin/delete.log
#echo "|"
#echo "|"
#echo "|"
#echo "KEY LOG: "
#cat $CI_PROJECT_DIR/bin/key.log
#echo "|"
#echo "|"
#echo "|"
#echo "ASSIGN LOG: "
#cat $CI_PROJECT_DIR/bin/assign.log
