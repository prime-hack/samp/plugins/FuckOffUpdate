#include "main.h"
#include <samp.hpp>
#include <BitStream.h>
#include <RakClient.h>

using namespace std::chrono_literals;

AsiPlugin::AsiPlugin() : SRDescent( nullptr ) {
	// Constructor
	spawn.onBefore += std::tuple{ this, &AsiPlugin::onSpawn };
	spawn.install( 0, 0, false );

	if ( SAMP::isR3() ) showDialog.changeAddr( 0xF96B );
	showDialog.onBefore += std::tuple{ this, &AsiPlugin::onShowDialog };
	showDialog.install( 12, 0, false );

	g_class.events->onMainLoop += std::tuple{ this, &AsiPlugin::onMainloop };
}

AsiPlugin::~AsiPlugin() {
	// Destructor
	SAMP::Base::DeleteInstance();
}

void AsiPlugin::onSpawn() {
	if ( lastSpawnTime == 0ms ) lastSpawnTime = tick();
}

void AsiPlugin::onShowDialog( SRHook::Info &info, char *&rightBtn ) {
	auto type = info.cpu.EDX;
	auto id = info.cpu.EAX;
	if ( type != 0 ) return;
	if ( *rightBtn != '\0' ) return;
	auto it = std::find( blockList.begin(), blockList.end(), id );
	if ( it != blockList.end() || tick() - lastSpawnTime < 5s ) {
		info.skipOriginal = true;
		info.cpu.ESP += 16;
		info.retAddr += 8;
		lastBlockedDialog = id;
		blockDialogTime = tick();
		if ( it == blockList.end() ) blockList.push_back( id );
		if ( SAMP::Base::Instance()->Dialog()->isActive() && SAMP::Base::Instance()->Dialog()->isRemote() )
			SAMP::Base::Instance()->Dialog()->setActive( false );
	}
}

void AsiPlugin::onMainloop() {
	if ( lastBlockedDialog != -1 && tick() - blockDialogTime > 100ms ) {
		BitStream respone;
		respone.Write( (uint16_t)lastBlockedDialog ); // id диалога
		respone.Write( (uint8_t)1 ); // id кнопки на диалоге
		respone.Write( (uint16_t)-1 ); // id элемента в списке
		respone.Write( (uint8_t)0 ); // длина текста
		//		respone.Write( "", 0 ); // текст

		int RPC_DialogResponse = 62;
		SAMP::Base::Instance()->pRakClient()->RPC( &RPC_DialogResponse, &respone, HIGH_PRIORITY, RELIABLE_ORDERED, 0, 0 );
		lastBlockedDialog = -1;
	}
}

std::chrono::milliseconds AsiPlugin::tick() {
	return std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now().time_since_epoch() );
}
