#ifndef MAIN_H
#define MAIN_H

#include "loader/loader.h"
#include <SRDescent/SRDescent.h>
#include <chrono>
#include <SRHook.hpp>

class AsiPlugin : public SRDescent {
	SRHook::Hook<> spawn{ 0x3AD0, 6, "samp" };
	SRHook::Hook<char *> showDialog{ 0xCD6B, 6, "samp" };

	std::chrono::milliseconds lastSpawnTime{ 0 };
	std::vector<int> blockList;

	int lastBlockedDialog = -1;
	std::chrono::milliseconds blockDialogTime{ 0 };

public:
	explicit AsiPlugin();
	virtual ~AsiPlugin();

protected:
	void onSpawn();
	void onShowDialog( SRHook::Info &info, char *&rightBtn );
	void onMainloop();

private:
	std::chrono::milliseconds tick();
};

#endif // MAIN_H
